btns={[
                    {
                        id: 0,
                        screen: constants.TABS_SCREEN.HOME,
                        xml: iconsSvg.home,
                        xmlActive: iconsSvg.homeActive,
                        badge: false,
                    },
                    {
                        id: 1,
                        screen: constants.TABS_SCREEN.LIKE,
                        xml: iconsSvg.love,
                        xmlActive: iconsSvg.loveActive,
                        badge: false,
                    },
                    {
                        id: 3,
                        screen: constants.TABS_SCREEN.CART,
                        xml: iconsSvg.cart,
                        xmlActive: iconsSvg.cartActive,
                        badge: true,
                    },
                    {
                        id: 4,
                        screen: constants.TABS_SCREEN.USER,
                        // xml: iconsSvg.back,
                        // xmlActive: iconsSvg.back,
                        url: this.props.user?.avatar,
                        badge: false,
                    },
                ]}
