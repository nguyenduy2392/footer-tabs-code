/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { Footer, FooterTab, View, Text } from 'native-base';
import { StyleSheet, SafeAreaView } from 'react-native'
import { Button, Avatar } from 'react-native-elements'
import styles, { circleWidth } from './styles';
import themes from '../../style/themes';
import LinearGradientButton from '../common/LinearGradientButton';
import Svg, { Circle, Path } from 'react-native-svg';
import * as shape from "d3-shape";
import iconsSvg from '../../helper/iconsSvg';
import IconWithBadgeAlert from '../common/IconWithBadgeAlert';
import { chunk } from 'lodash';
import Icon from 'react-native-vector-icons/Fontisto'
import constants from '../../constants';
const width = themes.screen.width
const height = themes.defaultFooterHeight;

const getPath = (): string => {
  const left = shape.line().x(d => d.x).y(d => d.y)([
    { x: 0, y: 0 },
    { x: width, y: 0 },
  ]);
  const tab = shape.line().x(d => d.x).y(d => d.y).curve(shape.curveBasis)([
    { x: 0 - 10, y: 0 }, // end left
    { x: 0 - 10, y: 0 },
    { x: 0 - 10, y: height },
    { x: 0 - 10, y: height },
    { x: 0 - 10, y: height },
    { x: 0, y: height },
    { x: 0, y: height },
    { x: 0, y: height },

    { x: 0, y: 20 },// border left
    { x: 0, y: 20 },
    { x: 0 + 2, y: 10 },
    { x: 0 + 10, y: 2 },
    { x: 0 + 20, y: 0 },
    { x: 0 + 20, y: 0 },

    { x: (width - circleWidth) / 2 - 20, y: 0 },// border center left
    { x: (width - circleWidth) / 2 - 20, y: 0 },
    { x: (width - circleWidth) / 2 - 10, y: 2 },
    { x: (width - circleWidth) / 2 - 2, y: 10 },
    { x: (width - circleWidth) / 2, y: 20 },

    { x: (width - circleWidth) / 2 + 10, y: 30 },// border center bottom lelf
    { x: (width - circleWidth) / 2 + 20, y: 36 },

    { x: (width - circleWidth) / 2 + 40, y: 40 },// border center bottom center

    { x: (width - circleWidth) / 2 + circleWidth - 20, y: 36 },// border center bottom right
    { x: (width - circleWidth) / 2 + circleWidth - 10, y: 30 },

    { x: (width - circleWidth) / 2 + circleWidth, y: 20 },// border center right
    { x: (width - circleWidth) / 2 + circleWidth + 2, y: 10 },
    { x: (width - circleWidth) / 2 + circleWidth + 10, y: 2 },
    { x: (width - circleWidth) / 2 + circleWidth + 20, y: 0 },
    { x: (width - circleWidth) / 2 + circleWidth + 20, y: 0 },

    { x: width - 20, y: 0 }, //border right
    { x: width - 20, y: 0 },
    { x: width - 10, y: 2 },
    { x: width - 2, y: 10 },
    { x: width, y: 20 },
    { x: width, y: 20 },
    { x: width, y: 0 },
    { x: width, y: 0 },

    { x: width, y: height }, // end left
    { x: width, y: height },
    { x: width, y: height },
    { x: width + 10, y: height },
    { x: width + 10, y: height },
    { x: width + 10, y: height },
    { x: width + 10, y: 0 },
    { x: width + 10, y: 0 },
  ]);
  const right = shape.line().x(d => d.x).y(d => d.y)([
    { x: width + circleWidth, y: 0 },
    { x: width * 2, y: 0 },
    { x: width * 2, y: height },
    { x: 0, y: height },
    { x: 0, y: 0 },
  ]);
  return `${left} ${tab} ${right}`;
};
const d = getPath();

type Props = {
  left: bool,
  right: bool,
  name: string,
  menu: bool,
  notification: bool,
  search: bool,
  bag: bool,
  badgeCount: number
};

class FooterTabs extends React.PureComponent<Props> {
  constructor(props: any) {
    super(props);
    this.state = {
      // activeTab: this.props.btns[0].screen
    }
  }

  onTab = (item) => () => {
    if (this.props.activeTab != item.screen) {
      // this.setState({
      //   activeTab: index
      // })
      this.props.onChangeTabs(item.screen, item.id)
    }
  }

  renderRight = (item, index) => {
    return <Button
      type="clear"
      containerStyle={styles.btnContainer}
      buttonStyle={styles.btnButton}
      onPress={this.onTab(item)}
      icon={
        item.xml && item.xmlActive
          ? <IconWithBadgeAlert
            xml={this.props.activeTab == item.screen ? item.xmlActive : item.xml}
            badge={item.badge ? this.props.cartsBadge : 0}
            size={25}
          />
          : item.url
            ? <Avatar
              size={28}
              rounded
              source={{ uri: item.url }}
              containerStyle={this.props.activeTab == item.screen ? styles.avatarActive : styles.avatar}
              imageProps={{
                resizeMethod: 'resize'
              }}
            />
            : <Avatar
              size={28}
              rounded
              renderPlaceholderContent={
                <Icon
                  name={'male'}
                  size={24}
                  color={themes.border}
                />
              }
              containerStyle={this.props.activeTab == item.screen ? styles.avatarActive : styles.avatar}
              imageProps={{
                resizeMethod: 'resize'
              }}
            />
      }
      key={index.toString()}
    />
  }

  onTabCenter = () => {
    if (this.props.activeTab != constants.TABS_SCREEN.CATEGORIES) {
      // this.setState({
      //   activeTab: constants.TABS_SCREEN.CATEGORIES
      // })
      this.props.onChangeTabs(constants.TABS_SCREEN.CATEGORIES, 2)
    }
  }

  render() {
    // const value = 0
    // const translateX = value.interpolate({
    //   inputRange: [0, width],
    //   outputRange: [-width, 0],
    // });
    this.props.btns
    let btns = chunk(this.props.btns, 2)
    return (
      <View style={styles.container}>
        <View>
          <Svg width={width} {...{ height }}>
            <Path fill={themes.bgHover} {...{ d }} />
          </Svg>
          <View style={styles.btns}>
            <View style={styles.btnLeft}>
              {btns[0].map(this.renderRight)}
            </View>
            <Button
              type="clear"
              containerStyle={styles.btnContainerCenter}
              buttonStyle={styles.btnButtonCenter}
              onPress={this.onTabCenter}
              icon={
                <IconWithBadgeAlert
                  xml={iconsSvg.filter}
                  size={30}
                />
              }
            />
            <View style={styles.btnRight}>
              {btns[1].map(this.renderRight)}
            </View>
          </View>
        </View>
        <SafeAreaView style={styles.safe} />
      </View>
    );
  }
}



export default FooterTabs;

