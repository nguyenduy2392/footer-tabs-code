import { StyleSheet, Dimensions } from 'react-native';
import themes from '../../style/themes';

// const contentWidth = Dimensions.get('window').width;
// const fullBtnWidth = contentWidth - themes.defaultPaddingPage * 2;
// const haffBtnWidth = (contentWidth - themes.defaultPaddingPage * 3) / 2;
const tabWidth = themes.screen.width / 5
const centerButton = 60
export const circleWidth = centerButton + 16


export default StyleSheet.create({
    container: {
        position: 'absolute',
        // height: themes.defaultFooterHeight,
        width: themes.screen.width,
        bottom: 0
    },
    safe: {
        backgroundColor: themes.bgHover
    },
    avatar: {
        backgroundColor: '#D1D6D9',
        borderWidth: 2,
        borderColor: themes.white,
    },
    avatarActive: {
        backgroundColor: '#D1D6D9',
        borderWidth: 2,
        borderColor: themes.oranger,
    },
    btnContainer: {
        width: tabWidth,
        height: themes.defaultFooterHeight,
        
    },
    btnButton: {
        width: tabWidth,
        height: themes.defaultFooterHeight,
        backgroundColor: 'rgba(0,0,0,0)'
    },
    btnContainerCenter: {
        width: centerButton,
        height: centerButton,
        borderRadius: centerButton / 2,
        // overflow: 'hidden',
        // borderWidth: 3,
        // borderColor: themes.black,
        bottom: centerButton / 2
    },
    btnButtonCenter: {
        width: centerButton,
        height: centerButton,
        borderRadius: centerButton / 2,
        backgroundColor: themes.oranger,
        borderWidth: 4,
        borderColor: themes.black,
    },
    btns: {
        width: themes.screen.width,
        position: 'absolute',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    btnLeft: {
        flexDirection: 'row',
    },
    btnRight: {
        flexDirection: 'row',
    },
});
